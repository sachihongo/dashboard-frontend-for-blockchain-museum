import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColectorComponent } from './colector.component';

describe('ColectorComponent', () => {
  let component: ColectorComponent;
  let fixture: ComponentFixture<ColectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
