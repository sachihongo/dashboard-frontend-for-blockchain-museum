import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LbdModule } from '../../lbd/lbd.module';
import { NguiMapModule} from '@ngui/map';

import { AdminLayoutRoutes } from './admin-layout.routing';

import { HomeComponent } from '../../home/home.component';
import { UserComponent } from '../../user/user.component';
import { TablesComponent } from '../../tables/tables.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { NewsComponent } from '../../news/news.component';
import { AddNewsComponent } from '../../add-news/add-news.component';
import { HolderComponent } from '../../holder/holder.component';
import { AssetComponent } from '../../asset/asset.component';
import { OwnershipComponent } from '../../ownership/ownership.component';
import { LogComponent } from '../../log/log.component';
import { ColectorComponent } from '../../colector/colector.component';
import { DevisorComponent } from '../../devisor/devisor.component';
import { OrganizationComponent } from '../../organization/organization.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    LbdModule,
    NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE'})
  ],
  declarations: [
    HomeComponent,
    NewsComponent,
    AddNewsComponent,
    HolderComponent,
    AssetComponent,
    OwnershipComponent,
    LogComponent,
    UserComponent,
    TablesComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    ColectorComponent,
    DevisorComponent,
    OrganizationComponent
  ]
})

export class AdminLayoutModule {}
