import { Routes } from '@angular/router';

import { HomeComponent } from '../../home/home.component';
import { UserComponent } from '../../user/user.component';
import { TablesComponent } from '../../tables/tables.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { NewsComponent } from '../../news/news.component';
import { AddNewsComponent } from '../../add-news/add-news.component';
import { HolderComponent } from '../../holder/holder.component';
import { AssetComponent } from '../../asset/asset.component';
import { OwnershipComponent } from '../../ownership/ownership.component';
import { LogComponent } from '../../log/log.component';
import { ColectorComponent } from '../../colector/colector.component';
import { DevisorComponent } from '../../devisor/devisor.component';
import { OrganizationComponent } from '../../organization/organization.component';


export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: HomeComponent },
    { path: 'news',           component: NewsComponent },
    { path: 'addnews',       component: AddNewsComponent },
    { path: 'holder',         component: HolderComponent },
    { path: 'colector',         component: ColectorComponent },
    { path: 'devisor',         component: DevisorComponent },
    { path: 'organization',         component: OrganizationComponent },
    { path: 'asset',          component: AssetComponent },
    { path: 'ownership',      component: OwnershipComponent },
    { path: 'log',            component: LogComponent },
    { path: 'user',           component: UserComponent },
    { path: 'table',          component: TablesComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'notifications',  component: NotificationsComponent }
];
