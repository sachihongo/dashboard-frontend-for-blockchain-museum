import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevisorComponent } from './devisor.component';

describe('DevisorComponent', () => {
  let component: DevisorComponent;
  let fixture: ComponentFixture<DevisorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevisorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
